package com.firewood.pary;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firewood.pary.Controller.MainController;
import com.firewood.pary.Model.UserModel;
import com.firewood.pary.Util.Util;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class JoinActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ArrayAdapter adapter;
    EditText name;
    EditText age;
    CheckBox privateAgreement;
    CheckBox serviceAgreement;
    Button   detailView;
    Button   detailView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);


        Typeface type = Typeface.createFromAsset(this.getAssets(), "JejuGothicOTF.otf");
        Util.setGlobalFont(getWindow().getDecorView(), type);

        MainController.Gender = "M";

        name = (EditText)findViewById(R.id.name);
        int color = Color.parseColor("#FFFFFF");
        name.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        age = (EditText)findViewById(R.id.age);
        age.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        final Spinner spinner = (Spinner)findViewById(R.id.genderSpinner);
        spinner.getBackground().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);

        adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setSelection(0);
        //이벤트를 일으킨 위젯과 리스너와 연결
        spinner.setOnItemSelectedListener(this);

        Button complete = (Button)findViewById(R.id.complete);
        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(name.getText().length() < 1)
               {
                   ShowError("이름을 입력하세요.");
                   return;
               }

                if(age.getText().length()  < 1)
                {
                    ShowError("나이를 입력하세요.");
                    return;
                }

                if(!serviceAgreement.isChecked())
                {
                    ShowError("서비스 이용관에 동의하세요.");
                    return;
                }

                if(!privateAgreement.isChecked())
                {
                    ShowError("개인정보 취급방침에 동의하세요.");
                    return;
                }

                DialogSimple();
            }
        });

        privateAgreement = (CheckBox)findViewById(R.id.privateAgreement);
        serviceAgreement = (CheckBox)findViewById(R.id.serviceAgreement);

        detailView = (Button)findViewById(R.id.detailView);
        detailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1nofgCc30UXFvIwxpXz-gACXbvKkZSIVjn1y3xsBDaXg/edit?usp=sharing"));
                // intent.setPackage("com.android.chrome");   // 브라우저가 여러개 인 경우 콕 찍어서 크롬을 지정할 경우
                startActivity(intent);
            }
        });

        detailView2 = (Button)findViewById(R.id.detailView2);
        detailView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1nofgCc30UXFvIwxpXz-gACXbvKkZSIVjn1y3xsBDaXg/edit?usp=sharing"));
                // intent.setPackage("com.android.chrome");   // 브라우저가 여러개 인 경우 콕 찍어서 크롬을 지정할 경우
                startActivity(intent);
            }
        });
    }


    private void DialogSimple(){
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("이대로 진행하시겠습니까?").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'Yes' Button


                        SharedPreferences pref = getSharedPreferences("Profile", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("gender", MainController.Gender);
                        editor.putString("name", name.getText().toString());
                        editor.putString("age", age.getText().toString());
                        editor.putBoolean("usePush", true);

                        MainController.Name = name.getText().toString();
                        MainController.Age = age.getText().toString();
                        MainController.usePush = true;

                        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
                        DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();


                        UserModel model = new UserModel(mFirebaseUser.getUid(), MainController.Name, MainController.Age, MainController.Gender, "On");
                        mFirebaseDatabaseReference.child("user").child(model.getFbid()).setValue(model);

                        editor.commit();
                        ShowMain();
                        finish();
                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog

        alert.setTitle("확인");



        alert.show();
    }

    private  void ShowMain()
    {
        final Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void ShowError(String value)
    {
        Toast.makeText(this, value, Toast.LENGTH_LONG).show();
    }

    //아이템 중 하나를 선택 했을때 호출되는 콜백 메서드
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        ((TextView)parent.getChildAt(0)).setTextColor(Color.WHITE);
        ((TextView)parent.getChildAt(0)).setTextSize(18);

            if(position == 0)
            {
                MainController.Gender = "M";
            }
            else
            {
                MainController.Gender = "F";
            }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
