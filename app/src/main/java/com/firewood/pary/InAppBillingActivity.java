package com.firewood.pary;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class InAppBillingActivity extends AppCompatActivity {

    InAppBillingHelper mHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_app_billing);

        Button buy = (Button)findViewById(R.id.buy);
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              mHelper.buy("pray_vip");
            }
        });

        mHelper = new InAppBillingHelper(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhogRwBw97YBfzonI9qbb46vbwInTBJIa/SyvfCbRmWruymHwnrMndHB698Mb7wp0lLhsLx1creEKEDl6bkwciLIEKKRBSVHAvGFLqzTASmeR9IC4kxxKtvl1OOmfqeTPB2Q7W9tqREDYG6kkL8RPU8g15mXURPxLjKzCUFEUiLPv2y6dllglQtnH5JRQI0oFWBrvkGc5cwBrvyNYeh8TXsx2jSepvyhqmOmuVejbdVZ9Cf5xP+//PEpyVJ4oSwqjSwsTLdbJylOokErnAhgnueUVie90Nj+MfQp+Hhah7D3iJRzgESXheNdFQ4lw9kBDut1AVLwABO4PQe9ThLP8QQIDAQAB")
        {
            @Override
            public void addInventory()
            {

            }
        };

        Button consume = (Button)findViewById(R.id.consume);
        consume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelper.GetPurchaseItems();
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mHelper.destroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        mHelper.activityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

}
