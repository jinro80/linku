package com.firewood.pary;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firewood.pary.Adapters.firebaseAdapter;
import com.firewood.pary.Controller.MainController;
import com.firewood.pary.Listeners.ClickListenerChatFirebase;
import com.firewood.pary.Model.ChatModel;
import com.firewood.pary.Model.UserModel;
import com.firewood.pary.Util.Util;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Chris on 2016-10-17.
 */

public class ChatActivity extends AppCompatActivity implements ClickListenerChatFirebase, View.OnClickListener
{
    static String CHAT_REFERENCE = "2";
    public static boolean isStart = false;
    private  boolean isTaking = true;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseDatabaseReference;

    //CLass Model
    private UserModel userModel;

    //Views UI
    private RecyclerView rvListMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageView btSendMessage;
    private EditText edMessage;
    private static TextView distance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLUE);
        }


        setContentView(R.layout.chat_activity);

        Typeface type = Typeface.createFromAsset(this.getAssets(), "JejuGothicOTF.otf");
        Util.setGlobalFont(getWindow().getDecorView(), type);

        CHAT_REFERENCE = getIntent().getStringExtra("chat");



        distance = (TextView) findViewById(R.id.distance);

        bindViews();
        InitailUser();

        if(CHAT_REFERENCE.equals("freeboard")) {

            distance.setVisibility(View.GONE);
        }
        else {
            mFirebaseDatabaseReference.child("Room").child(CHAT_REFERENCE).addChildEventListener(childEventListener);
            mFirebaseDatabaseReference.child("Room").child(CHAT_REFERENCE).onDisconnect().removeValue();
            mFirebaseDatabaseReference.child("message").child(CHAT_REFERENCE).onDisconnect().removeValue();

            SetDistanceInfo();
        }

        isStart = true;
    }

    public  static   void SetDistanceInfo()
    {
        //float[] results = new float[3];

        Location locationA = new Location("A");
        locationA.setLatitude(MainController.Lat);
        locationA.setLongitude(MainController.Lon);

        Location locationB = new Location("B");
        locationB.setLatitude(MainController.Lat2);
        locationB.setLongitude(MainController.Lon2);

        float value =  locationA.distanceTo(locationB) / 1000;

        //Location.distanceBetween(lat, lon, lat2, lon2, results);


        if(MainController.Lat == 0)
        {
            return;
        }

        if(MainController.Lat2 == 0)
        {
            return;
        }

        if(MainController.Lon == 0)
        {
            return;
        }

        if(MainController.Lon2 == 0)
        {
            return;
        }

        if(value > 0)
        {
            distance.setText("상대방과 " + String.format("%.2f" , value) + " km  떨어져 있습니다." );
        }
    }



    private  void ShowMeg()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }
        });

        alert.setCancelable(false);
        alert.setMessage("대화 상대가 대화방을 떠났습니다.");
        alert.show();
    }

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {



        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            if(isTaking) {
                isTaking = false;
                ShowMeg();
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            //ShowMeg();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            String value = databaseError.toString();
            Log.d("databaseError", value);
        }
    };




    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonMessage:
                sendMessageFirebase();
                break;
        }
    }

    @Override
    public void clickImageChat(View view, int position,String nameUser,String urlPhotoUser,String urlPhotoClick) {
       /* Intent intent = new Intent(this,FullScreenImageActivity.class);
        intent.putExtra("nameUser",nameUser);
        intent.putExtra("urlPhotoUser",urlPhotoUser);
        intent.putExtra("urlPhotoClick",urlPhotoClick);
        startActivity(intent);*/
    }

    @Override
    public void clickImageMapChat(View view, int position,String latitude,String longitude) {
       /* String uri = String.format("geo:%s,%s?z=17&q=%s,%s", latitude,longitude,latitude,longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);*/
    }

    /**
     * Enviar msg de texto simples para chat
     */
    private void sendMessageFirebase(){

        java.util.Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String current = formatter.format(today);


            ChatModel model = new ChatModel(userModel, edMessage.getText().toString(), current);
            mFirebaseDatabaseReference.child("message").child(CHAT_REFERENCE).push().setValue(model);


        edMessage.setText(null);
    }

    private void bindViews(){
        edMessage = (EditText)findViewById(R.id.editTextMessage);
        btSendMessage = (ImageView)findViewById(R.id.buttonMessage);
        btSendMessage.setOnClickListener(this);
        rvListMessage = (RecyclerView)findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
    }


    private void InitailUser(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null){

            finish();
        }else{
            userModel = new UserModel(mFirebaseUser.getUid(), MainController.Name, MainController.Age, MainController.Gender,"On");
            userModel.setLat(MainController.Lat);
            userModel.setLon(MainController.Lon);
            initailizeMessage();
        }
    }

    private void initailizeMessage(){
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        final firebaseAdapter firebaseAdapter = new firebaseAdapter(mFirebaseDatabaseReference.child("message").child(CHAT_REFERENCE), userModel, this);
        firebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = firebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    rvListMessage.scrollToPosition(positionStart);
                }
            }
        });


        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.setAdapter(firebaseAdapter);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();


        if(!isTaking)
        {
            mFirebaseDatabaseReference.child("message").child(CHAT_REFERENCE).removeValue();
        }


        isTaking = false;
        mFirebaseDatabaseReference.child("Room").child(CHAT_REFERENCE).removeValue();

        isStart = false;

    }
}
