package com.firewood.pary.Util;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Chris on 2016-12-20.
 */

public class Util {

    public static void setGlobalFont(View view, Typeface type) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup)view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(type);
                    }
                    setGlobalFont(v, type);
                }
            }
        }
    }
}
