package com.firewood.pary.Model;

/**
 * Created by Chris on 2016-10-17.
 */

public class ChatModel {

    private String fbid;
    private String text;
    private String time;
    private  String name;

    //private  String photo_profile;


    public ChatModel() {
    }

    public ChatModel(UserModel userModel, String text, String time) {

        this.text = text;
        this.fbid = userModel.getFbid();
        this.time = time;


        String gender = "여";

        if(userModel.getGender().equals("M"))
        {
            gender = "남";
        }

        this.name = userModel.getName() + " (" + gender +", "+ userModel.getAge() +")";
        //this.photo_profile = userModel.getPhoto_profile();
    }

    
    public String getFbid() {
        return fbid;
    }

    public void setFbid(String id) {
        this.fbid = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName()
    {
        return this.name;
    }
    public  void setName(String name){this.name = name;}

    /*public  String getPhoto_profile()
    {
        return  this.photo_profile;
    }

    public  void setPhoto_profile(String photo_profile)
    {
        this.photo_profile = photo_profile;
    }*/

/*    @Override
    public String toString() {
        return "ChatModel{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", text='" + text + '\'' +
                '}';
    }*/
}
