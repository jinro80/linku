package com.firewood.pary.Model;

import com.firewood.pary.Controller.MainController;
import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chris on 2016-10-17.
 */

public class UserModel {

    private String fbid;
    private String name;
    //private String photo_profile;
    private String gender = "M";
    private  String status = "Off";
    private  String age = "20";
    private  double lat = 0.0;
    private  double lon = 0.0;
    private  String start_time;
    private  String end_time;
    private  String chatType = "A";

    public UserModel() {
    }

    public UserModel(String fbid, String name, String age, String gender,  String status ){
        this.name = name;
        this.age = age;
        this.gender = gender;
        //this.photo_profile = photo_profile;
        this.fbid = fbid;
        this.status = status;
    }

    public UserModel(String fbid, String name, String age, String gender, String status, String chatType ){
        this.name = name;
        this.age = age;
        this.gender = gender;
        //this.photo_profile = photo_profile;
        this.fbid = fbid;
        this.status = status;
        this.chatType = chatType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public String getPhoto_profile() {
        return photo_profile;
    }

    public void setPhoto_profile(String photo_profile) {
        this.photo_profile = photo_profile;
    }*/

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String id) {
        this.fbid = fbid;
    }

    public  void  setGender(String gender)
    {
        this.gender = gender;
    }

    public String getGender()
    {
        return this.gender;
    }

    public  void setStatus(String status)
    {
        this.status = status;
    }

    public  String getStatus()
    {
        return  this.status;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    public String getAge()
    {
        return  this.age;
    }

    public  void setLat(double lat)
    {
        this.lat = lat;
    }

    public  double getLat(){
        return  this.lat;
    }

    public  void setLon(double lon)
    {
        this.lon = lon;
    }
    public  double getLon()
    {
        return  this.lon;
    }

    public  void setStart_time(String start_time)
    {
        this.start_time = start_time;
    }

    public String getStart_time()
    {
        return this.start_time;
    }

    public  void setEnd_time(String end_time)
    {
        this.end_time = end_time;
    }

    public  String getEnd_time()
    {
        return this.end_time;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fbid", fbid);
        result.put("name", name);
        //result.put("photo_profile", photo_profile);
        result.put("gender", gender);
        result.put("status", status);
        result.put("age", age);
        result.put("lat", lat);
        result.put("lon", lon);
        result.put("start_time", start_time);
        result.put("end_time", end_time);
        result.put("chat_type", chatType);
        result.put("token", MainController.token );
        return result;
    }

    @Exclude
    public Map<String, Object> toMapBasic() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fbid", fbid);
        result.put("name", name);
        //result.put("photo_profile", photo_profile);
        result.put("gender", gender);
        result.put("status", status);
        result.put("age", age);
        result.put("lat", lat);
        result.put("lon", lon);
        result.put("start_time", start_time);
        result.put("end_time", end_time);    ;
        result.put("token", MainController.token );
        return result;
    }

}
