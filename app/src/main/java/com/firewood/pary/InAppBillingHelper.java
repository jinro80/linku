package com.firewood.pary;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.firewood.pary.Controller.MainController;
import com.firewood.pary.Util.IabHelper;
import com.firewood.pary.Util.IabResult;
import com.firewood.pary.Util.Purchase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.android.vending.billing.*;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Chris on 2016-12-19.
 */

public abstract class InAppBillingHelper {

    private String          PUBLIC_KEY;

    private final int       REQUEST_CODE = 1001;

    private Activity mActivity;
    private IInAppBillingService    mService;
    private IabHelper mHelper;

    public InAppBillingHelper(Activity _act, String _public_key)
    {
        mActivity           = _act;
        PUBLIC_KEY          = _public_key;
        init();
    }

    ServiceConnection mServiceConn = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            mService        = IInAppBillingService.Stub.asInterface(service);

            getSku();

            GetPurchaseItems();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            mService        = null;
        }
    };

    public void getSku()
    {
        ArrayList<String> skuList = new ArrayList<String> ();
        skuList.add("pray_vip");

        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

        try {

            Bundle skuDetails = mService.getSkuDetails(3,
                    mActivity.getPackageName(), "inapp", querySkus);

            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String> responseList
                        = skuDetails.getStringArrayList("DETAILS_LIST");

                for (String thisResponse : responseList) {
                    JSONObject object = new JSONObject(thisResponse);
                    String sku = object.getString("productId");
                    String price = object.getString("price");

                }
            }
            else
            {
                String aa = "0";
            }
        }
        catch (Exception e)
        {
String bbb =e.toString();
        }
    }

    public void init()
    {
        // 패키지를 명시적으로 설정. ( => 모든 버전의 안드로이드에서 작동시키기 위함.)
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        mActivity.bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);

        mHelper             = new IabHelper(mActivity, PUBLIC_KEY);
        mHelper.enableDebugLogging(true);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()
        {
            @Override
            public void onIabSetupFinished(IabResult result)
            {
                if (!result.isSuccess())
                {
                    // TODO:: 구매오류처리 (토스트 하나 띄우고 결제팝업 종료시키면 됨)
                    Toast.makeText(mActivity, "구매 실패", Toast.LENGTH_SHORT).show();
                }
                // 구매목록을 초기화하는 메서드.
                // v3으로 넘어오면서 구매기록이 모두 남게 되는데, 재구매 가능한 상품( 게임에서는 코인같은 아잍메은 ) 구매후 삭제해줘야 함.
                // 이 메서드는 상품 구매전 혹은 후에 반드시 호출해야한다. ( 재구매가 불가능한 1회성 아이템의경우 호출하면 안됨. )
                //AlreadyPurchaseItems();
            }
        });
    }

    public void destroy()
    {
        if (mServiceConn != null)
            mActivity.unbindService(mServiceConn);
    }

    public void GetPurchaseItems()
    {
        try
        {
            Log.d("구매" , "구매 목록 확인 시작");

            Bundle ownedItems = mService.getPurchases(3, mActivity.getPackageName(), "inapp", null);
            int             response = ownedItems.getInt("RESPONSE_CODE");

            if (response == 0)
            {
                Log.d("구매" , "구매 목록 확인 성공");

             /*   ArrayList<String> ownedSkus =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");*/
                ArrayList<String>  purchaseDataList =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
               /* ArrayList<String>  signatureList =
                        ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                String continuationToken =
                        ownedItems.getString("INAPP_CONTINUATION_TOKEN");*/


                String[] tokens = new String[purchaseDataList.size()];

                Log.d("구매목록 숫자  : ", String.valueOf(purchaseDataList.size()));

                for (int i = 0; i < purchaseDataList.size(); ++i)
                {
                    String purchaseData = (String)purchaseDataList.get(i);
                    JSONObject jo   = new JSONObject(purchaseData);
                    tokens[i]   = jo.getString("purchaseToken");
                    // 여기서 tokens를 모두 컨슘 해주기

                    //0 : 구매, 1 : 취소, 2: 환불
                    String purchaseState = jo.getString("purchaseState");
                    String  purchaseTime = jo.getString("purchaseTime");

                    Date date = new Date(Long.valueOf(purchaseTime));
                    Date now = new Date();


                    long diff = now.getTime() - date.getTime();
                    long diffDays = diff / (24 * 60 * 60 * 1000);


                    String dateString = new SimpleDateFormat("yyyy/MM/dd").format(date);

                    Log.d("구매목록", "purchaseState : " + purchaseState.toString() + "   purchaseTime : "+ dateString);
                    Log.d("구매 경과" , "diff Days : " + String.valueOf(diffDays).toString());


                    if(purchaseState.equals("0")) { //구매한 상태
                        if (diffDays > 30) {
                            //결제 만료...

                            ConsumePurchaseItems();
                            MainController.VIP = false;

                            Log.d("구매" , "구매 기한 만료");

                        } else {

                            Log.d("구매" , "유효한 구매");
                            MainController.VIP = true;
                            MainController.purchaseDate = dateString;
                            MainController.remainDate = 30 - (int)diffDays;
                            break;
                            //결제 유효.
                        }
                    }
                    else
                    {
                        //취소나 환불 상태.
                        Log.d("구매" , "취소나 환불된 상태");
                        ConsumePurchaseItems();
                        MainController.VIP = false;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.d("구매 목록 확인 오류" , e.getMessage().toString());
            e.printStackTrace();
        }
    }


    public void ConsumePurchaseItems()
    {
        try
        {
            Bundle ownedItems = mService.getPurchases(3, mActivity.getPackageName(), "inapp", null);
            int             response = ownedItems.getInt("RESPONSE_CODE");

            if (response == 0)
            {

      /*          ArrayList<String> ownedSkus =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");*/
                ArrayList<String>  purchaseDataList =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
               /* ArrayList<String>  signatureList =
                        ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");*/
            /*    String continuationToken =
                        ownedItems.getString("INAPP_CONTINUATION_TOKEN");*/


                String[] tokens = new String[purchaseDataList.size()];

                Log.d("구매목록 숫자  : ", String.valueOf(purchaseDataList.size()));

                for (int i = 0; i < purchaseDataList.size(); ++i)
                {
                    String purchaseData = (String)purchaseDataList.get(i);
                    JSONObject jo   = new JSONObject(purchaseData);
                    tokens[i]   = jo.getString("purchaseToken");
                    // 여기서 tokens를 모두 컨슘 해주기

                    String purchaseState = jo.getString("purchaseState");
                    String  purchaseTime = jo.getString("purchaseTime");

                    Date date = new Date(Long.valueOf(purchaseTime));
                    String dateString = new SimpleDateFormat("yyyy/MM/dd").format(date);
                    Toast.makeText(mActivity, "purchaseState : " + purchaseState + "   purchaseTime : "+ dateString, Toast.LENGTH_SHORT).show();

                    Log.d("구매목록", "purchaseState : " + purchaseState.toString() + "   purchaseTime : "+ dateString);


                    //여기서 컨슘하지 않으면 목록을 가지고있기때문에 날짜 비교후, 컨슘해주면 유지할수 있다.
                    //구매일로 30일이 경과했으면, 컨슘해주고 다시 구매가능하도록 설정해줘야한다.
                    mService.consumePurchase(3, mActivity.getPackageName(), tokens[i]);
                    MainController.VIP = false;
                }
            }
        }
        catch (Exception e)
        {
            Log.d("구매 소비 오류" , e.getMessage().toString());
            e.printStackTrace();
        }
    }


    public void buy(String _item)
    {
        try
        {
            Bundle buyIntentBundle  = mService.getBuyIntent(3, mActivity.getPackageName(), _item, "inapp", "test");     // 부정 방지를 위해 임의로 토큰 생성.
            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

            if (pendingIntent != null)
            {
                //mHelper.launchPurchaseFlow(mActivity, mActivity.getPackageName(), REQUEST_CODE, mPurchaseFinishedListener);
                mActivity.startIntentSenderForResult(pendingIntent.getIntentSender(),  REQUEST_CODE, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
            }
            else
            {
                // 결제가 막혔다면
                Toast.makeText(mActivity, "구매 실패", Toast.LENGTH_SHORT).show();
                ConsumePurchaseItems();
                MainController.VIP = false;
            }
        }
        catch (Exception e)
        {
            Toast.makeText(mActivity, "구매 실패", Toast.LENGTH_SHORT).show();
            ConsumePurchaseItems();
            MainController.VIP = false;
            e.printStackTrace();
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener   = new IabHelper.OnIabPurchaseFinishedListener()
    {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase info)
        {
            if (result.isFailure())
            {
                Log.d("구매 실패", result.getMessage());
                //AlreadyPurchaseItems();

                ConsumePurchaseItems();
                MainController.VIP = false;
            }
            else
            {

                String dateString = new SimpleDateFormat("yyyy/MM/dd").format(new Date(info.getPurchaseTime()));
                Log.d("구매", result.getMessage().toString());
                Log.d("구매 order id :", info.getOrderId().toString());
                Log.d("구매 state : ", String.valueOf(info.getPurchaseState()));
                Log.d("구매 time : ", dateString);


                // 여기서 아이템 추가 해주면 됨.
                addInventory();
                //AlreadyPurchaseItems();

                GetPurchaseItems();
            }
        }
    };

    // TODO:: 무명 클래스로 Override
    public abstract void addInventory();

    public void activityResult(int _requestCode, int _resultCode, Intent _data)
    {
        if (_requestCode == REQUEST_CODE) {
            int responseCode = _data.getIntExtra("RESPONSE_CODE", 0);

            if (_resultCode == RESULT_OK) {
                try {

                    GetPurchaseItems();
                }
                catch (Exception e) {
                    Log.d("구매 실패", e.getMessage());
                    e.printStackTrace();
                    ConsumePurchaseItems();
                    MainController.VIP = false;
                }
            }
        }

        // IabHelper가 null값인 경우 리턴.
        if (mHelper == null)
            return;

        // IabHelper가 데이터를 핸들링하도록 데이터 전달.
        if (!mHelper.handleActivityResult(_requestCode, _resultCode, _data))
            return;
    }

}
