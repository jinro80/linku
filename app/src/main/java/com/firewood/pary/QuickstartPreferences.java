package com.firewood.pary;

/**
 * Created by Chris on 2017-01-10.
 */

public class QuickstartPreferences {

    public static final String REGISTRATION_READY = "registrationReady";
    public static final String REGISTRATION_GENERATING = "registrationGenerating";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";


}
