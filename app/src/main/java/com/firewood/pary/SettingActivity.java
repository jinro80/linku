package com.firewood.pary;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firewood.pary.Controller.MainController;
import com.firewood.pary.Model.UserModel;
import com.firewood.pary.Util.Util;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SettingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ArrayAdapter adapter;
    EditText name;
    EditText age;
    TextView purchase;
    CheckBox usePush;


    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        Typeface type = Typeface.createFromAsset(this.getAssets(), "JejuGothicOTF.otf");
        Util.setGlobalFont(getWindow().getDecorView(), type);

        usePush = (CheckBox)findViewById(R.id.usePush);
        usePush.setChecked(MainController.usePush);

        name = (EditText)findViewById(R.id.name);
        int color = Color.parseColor("#FFFFFF");
        name.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        age = (EditText)findViewById(R.id.age);
        age.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        final Spinner spinner = (Spinner)findViewById(R.id.genderSpinner);
        spinner.getBackground().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);

        adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setSelection(0);
        //이벤트를 일으킨 위젯과 리스너와 연결
        spinner.setOnItemSelectedListener(this);

        Button complete = (Button)findViewById(R.id.complete);
        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(name.getText().length() < 1)
                {
                    ShowError("이름을 입력하세요.");
                    return;
                }

                if(age.getText().length()  < 1)
                {
                    ShowError("나이를 입력하세요.");
                    return;
                }

                DialogSimple();
            }
        });

        name.setText(MainController.Name);
        age.setText(MainController.Age);

        if(MainController.Gender == "M") {
            spinner.setSelection(0);
        }
        else
        {
            spinner.setSelection(1);
        }


        purchase = (TextView)findViewById(R.id.purchase);

        if(MainController.remainDate > 0)
        {
            purchase.setText("VIP 이용권 구매일 : " + MainController.purchaseDate + " (" + MainController.remainDate + "일 남음)");
        }
    }

    private void DialogSimple(){
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("이대로 진행하시겠습니까?").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'Yes' Button


                        SharedPreferences pref = getSharedPreferences("Profile", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("gender", MainController.Gender);
                        editor.putString("name", name.getText().toString());
                        editor.putString("age", age.getText().toString());
                        editor.putBoolean("usePush", usePush.isChecked());
                        MainController.Name = name.getText().toString();
                        MainController.Age = age.getText().toString();
                        MainController.usePush = usePush.isChecked();

                        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
                        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
                        DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();


                        UserModel model = new UserModel(mFirebaseUser.getUid(), MainController.Name, MainController.Age, MainController.Gender, "On");
                        mFirebaseDatabaseReference.child("user").child(model.getFbid()).setValue(model);

                        editor.commit();
                        ShowMain();
                        finish();
                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alt_bld.create();
        // Title for AlertDialog

        alert.setTitle("확인");



        alert.show();
    }

    private  void ShowMain()
    {
        final Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void ShowError(String value)
    {
        Toast.makeText(this, value, Toast.LENGTH_LONG).show();
    }

    //아이템 중 하나를 선택 했을때 호출되는 콜백 메서드
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        ((TextView)parent.getChildAt(0)).setTextColor(Color.WHITE);
        ((TextView)parent.getChildAt(0)).setTextSize(18);

        if(position == 0)
        {
            MainController.Gender = "M";
        }
        else
        {
            MainController.Gender = "F";
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
