package com.firewood.pary;

/**
 * Created by Chris on 2017-01-10.
 */

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.firewood.pary.Controller.MainController;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.List;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private static final String GCM_TOPIC_FOR_NOTICE = "notice-for-all";

    /**
     *
     * @param from SenderID 값을 받아온다.
     * @param data Set형태로 GCM으로 받은 데이터 payload이다.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {

    /*    if(MainController.isRunning)
        {
            return;
        }*/


        SharedPreferences pref = getSharedPreferences("Profile", Activity.MODE_PRIVATE);

        if(pref.getBoolean("usePush", true)) {

            ActivityManager actMng = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> list = actMng.getRunningAppProcesses();
            String packageName = "";
            for (ActivityManager.RunningAppProcessInfo rap : list) {
                System.out.println("packageName = " + packageName + ", importance = " + rap.importance);
                if (rap.importance == rap.IMPORTANCE_FOREGROUND) {
                    packageName = rap.processName;
                    System.out.println("packageName = " + packageName);
                    break;
                }
            }

            if(packageName.equals(this.getPackageName()))
            {
                return;
            }


            String title = data.getString("title");
            String message = data.getString("message");
            String message2 = data.getString("message2");
            String token = data.getString("sender_token");

            Log.d(TAG, "From: " + from);
            Log.d(TAG, "Title: " + title);
            Log.d(TAG, "Message: " + message);

            if (MainController.token.equals(token)) {
                return;
            }

            // GCM으로 받은 메세지를 디바이스에 알려주는 sendNotification()을 호출한다.
            sendNotification(title, message, message2);
        }
    }




    /**
     * 실제 디바에스에 GCM으로부터 받은 메세지를 알려주는 함수이다. 디바이스 Notification Center에 나타난다.
     * @param title
     * @param message
     */
    private void sendNotification(String title, String message, String message2) {
        Intent intent = new Intent(this, SplashActivityAnonymous.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);



        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_maps_location_history)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();

        inboxStyle.setBigContentTitle(title);
        inboxStyle.addLine(message);
        inboxStyle.addLine(message2);

        notificationBuilder.setStyle(inboxStyle);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
