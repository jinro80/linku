package com.firewood.pary.Adapters;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firewood.pary.Controller.MainController;
import com.firewood.pary.Listeners.ClickListenerChatFirebase;
import com.firewood.pary.Model.ChatModel;
import com.firewood.pary.Model.UserModel;
import com.firewood.pary.R;
import com.firewood.pary.Util.CircleTransform;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Chris on 2016-10-17.
 */

public class firebaseAdapter extends FirebaseRecyclerAdapter<ChatModel,firebaseAdapter.MyChatViewHolder> {

    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    private static final int RIGHT_MSG_IMG = 2;
    private static final int LEFT_MSG_IMG = 3;

    private ClickListenerChatFirebase mClickListenerChatFirebase;


    private UserModel mUser;


    public firebaseAdapter(DatabaseReference ref, UserModel user, ClickListenerChatFirebase mClickListenerChatFirebase) {
        super(ChatModel.class, R.layout.left_message_viewholder, firebaseAdapter.MyChatViewHolder.class, ref);
        this.mUser = user;
        this.mClickListenerChatFirebase = mClickListenerChatFirebase;
    }

    @Override
    public firebaseAdapter.MyChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RIGHT_MSG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.right_message_viewholder,parent,false);
            return new MyChatViewHolder(view);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.left_message_viewholder,parent,false);
            return new MyChatViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatModel model = getItem(position);

        String senderID = model.getFbid().trim().toString();

        Log.d("Chat ID 1", senderID);
        Log.d("Chat ID 2", mUser.getFbid().trim().toString());

    /*    if (model.getMapModel() != null){
            if (model.getId().trim().toString() == mUser.getId().trim().toString()){
                return RIGHT_MSG_IMG;
            }else{
                return LEFT_MSG_IMG;
            }
        }else if (model.getFile() != null){
            if (model.getFile().getType().equals("img") && model.getUserModel().getId().equals(mUser.getId())){
                return RIGHT_MSG_IMG;
            }else{
                return LEFT_MSG_IMG;
            }
        }else */


        if (senderID.equals(mUser.getFbid().trim().toString()))
        {
            return RIGHT_MSG;
        }
        else
        {

            return LEFT_MSG;
        }
    }



    @Override
    protected void populateViewHolder(MyChatViewHolder viewHolder, ChatModel model, int position) {
        //viewHolder.setIvUser(model.getPhoto_profile());
        if(getItemViewType(position) == LEFT_MSG) {
            //viewHolder.setUserId(model.getName());

            if(MainController.Name2.equals(""))
            {
                viewHolder.setUserId(model.getName());
            }
            else {

                viewHolder.setUserId(MainController.GetName2());
            }
        }
        else
        {
            viewHolder.setUserId("나");
        }
        viewHolder.setTxtMessage(model.getText());
        viewHolder.setTvTimestamp(model.getTime());
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTimestamp,tvLocation;
        TextView txtMessage;
        ImageView ivUser,ivChatPhoto;
        TextView userId;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvTimestamp = (TextView)itemView.findViewById(R.id.timestamp);
            txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
            ivUser = (ImageView)itemView.findViewById(R.id.ivUserChat);
            userId = (TextView)itemView.findViewById(R.id.userId);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ChatModel model = getItem(position);
           /* if (model.getMapModel() != null){
                mClickListenerChatFirebase.clickImageMapChat(view,position,model.getMapModel().getLatitude(),model.getMapModel().getLongitude());
            }else{
                mClickListenerChatFirebase.clickImageChat(view,position,model.getUserModel().getName(),model.getUserModel().getPhoto_profile(),model.getFile().getUrl_file());
            }*/
        }

        public  void setUserId(String id)
        {
            if(userId == null) return;
            userId.setText(id);
        }

        public void setTxtMessage(String message){
            if (txtMessage == null)return;
            txtMessage.setText(message);
        }

        public void setIvUser(String urlPhotoUser){
            if (ivUser == null)return;
            Glide.with(ivUser.getContext()).load(urlPhotoUser).centerCrop().transform(new CircleTransform(ivUser.getContext())).override(40,40).into(ivUser);
        }

        public void setTvTimestamp(String timestamp){
            if (tvTimestamp == null)return;
            tvTimestamp.setText(timestamp);
        }

        public void setIvChatPhoto(String url){
            if (ivChatPhoto == null)return;
            Glide.with(ivChatPhoto.getContext()).load(url)
                    .override(100, 100)
                    .fitCenter()
                    .into(ivChatPhoto);
            ivChatPhoto.setOnClickListener(this);
        }

        public void tvIsLocation(int visible){
            if (tvLocation == null)return;
            tvLocation.setVisibility(visible);
        }

    }

    private CharSequence converteTimestamp(String mileSegundos){
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSegundos),System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }

}
