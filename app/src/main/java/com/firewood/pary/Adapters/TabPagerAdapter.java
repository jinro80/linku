package com.firewood.pary.Adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.firewood.pary.Fragments.ChatListTabFragment;
import com.firewood.pary.Fragments.ChatSearchingTabFragment;


/**
 * Created by Chris on 2016-10-17.
 */

public class TabPagerAdapter  extends FragmentStatePagerAdapter {

    // Count number of tabs
    private int tabCount;

    public TabPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        // Returning the current tabs
        switch (position) {
            case 0:
                ChatSearchingTabFragment tabFragment1 = new ChatSearchingTabFragment();
                return tabFragment1;
            case 1:
                ChatListTabFragment tabFragment2 = new ChatListTabFragment();
                return tabFragment2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}