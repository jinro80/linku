package com.firewood.pary;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.firewood.pary.Auth.FaceBookLoginActivity;
import com.firewood.pary.Controller.MainController;
import com.firewood.pary.Model.UserModel;
import com.firewood.pary.Util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseDatabaseReference;
    private String mChatType = "A";
    private TextView femaleButton;
    private TextView maleButton;
    private TextView anyButton;
    private  TextView freeBoardButton;
    LocationManager mLM;
    InAppBillingHelper mHelper;
    Location location = null;
    private int adCount = 0;

    InterstitialAd main_inter;
    AdRequest adRequest;
    boolean isChat = false;

    private void ShowDialog()
    {
        mProgressDialog = ProgressDialog.show(this, "" ,"Loading...", true);
    }

    private void HideDialog()
    {
        if (mProgressDialog!=null&&mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    private void signInAnonymously() {
        ShowDialog();
        // [START signin_anonymously]
        mFirebaseAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

                        //mFirebaseDatabaseReference.child("Queue").addChildEventListener(userChildEventListener);



                        if (!task.isSuccessful()) {
                            ShowError();
                        }

                        // [START_EXCLUDE]
                        HideDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END signin_anonymously]
    }

    private  void ShowAd()
    {
        if (main_inter.isLoaded()) {
            main_inter.show();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
               ShowError();
            }
            return false;
        }
        return true;
    }

    public void getInstanceIdToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public void registBroadcastReceiver(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();


                if(action.equals(QuickstartPreferences.REGISTRATION_READY)){
                    // 액션이 READY일 경우



                } else if(action.equals(QuickstartPreferences.REGISTRATION_GENERATING)){
                    // 액션이 GENERATING일 경우

                } else if(action.equals(QuickstartPreferences.REGISTRATION_COMPLETE)){
                    // 액션이 COMPLETE일 경우

                    MainController.token = intent.getStringExtra("token");

                }

            }
        };
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registBroadcastReceiver();

        getInstanceIdToken();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }

        main_inter = new InterstitialAd(this);
        main_inter.setAdUnitId("ca-app-pub-1267642041690533/2953670604");
        adRequest = new AdRequest.Builder()
                .addTestDevice("696760246096179")
                .build();

        main_inter.loadAd(adRequest);

        main_inter.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }

            @Override
            public void onAdClosed() {

                main_inter.loadAd(adRequest);
                adCount = 0;
            }

        });

        mLM = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        SetLocation();

        Typeface type = Typeface.createFromAsset(this.getAssets(), "JejuGothicOTF.otf");
        Util.setGlobalFont(getWindow().getDecorView(), type);


        freeBoardButton = (TextView)findViewById(R.id.freeBoardButton);
        freeBoardButton.setTypeface(type);
        freeBoardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startFreeBoardActivity();

                if(!MainController.VIP) {
                    adCount++;
                }
            }
        });

        femaleButton = (TextView) findViewById(R.id.femaleButton);
        femaleButton.setTypeface(type);
        femaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(MainController.VIP) {
                    mChatType = "F";
                    startChat();
                }
                else
                {
                    ShowInAppBilling();
                }
            }
        });
        maleButton = (TextView)findViewById(R.id.maleButton);
        maleButton.setTypeface(type);
        maleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainController.VIP) {
                    mChatType = "M";
                    startChat();
                }
                else
                {
                    ShowInAppBilling();
                }
            }
        });

        anyButton = (TextView)findViewById(R.id.anyButton);
        anyButton.setTypeface(type);
        anyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mChatType = "A";
                startChat();

                if(!MainController.VIP) {
                    adCount++;
                }
            }
        });


        mHelper = new InAppBillingHelper(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm0KDO9C81yrAvMo1ThuJYfj8RqpNZwD5lh6elNJhfQdmHg2TbGuVI98+S/Hun1kua+Se9USjwVz3byTSXJuHIYTh76C9BJgu4lXTkCeCw8MiZaFboRoPJ5KZwRQUpj88y1+FHd9jYxoitU7DO4RvPOBCJ+K8meSkYhyI0Sscihs8z1JWw5PPzxNs9NJZm9APb7vnFY1A0v7kgwQonj+9QcfH51y4ToOcJYBzu2Y6PBTjuWRYfJigrPdf1ZPdSeyd3wUNN0Z693YaIzIE9HDa/47Qy+NEkRJPp65dZGDixkUvTn3zb+h09h6xPJJpcIyJ8p+JrQ1WU9ZYM+yjG/MpaQIDAQAB")
        {
            @Override
            public void addInventory()
            {

            }
        };



        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {

            signInAnonymously();

        } else {

            mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();


         /*   mFirebaseDatabaseReference.onDisconnect().removeValue(new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError error, DatabaseReference firebase) {
                    if (error != null) {
                        mFirebaseDatabaseReference.child("disconnect").push().setValue("I disconnnected");
                    }
                }
            });*/

            //mFirebaseDatabaseReference.child("Queue").addChildEventListener(userChildEventListener);
       /*     mFirebaseDatabaseReference.child("Room").orderByChild("1").equalTo(mFirebaseUser.getUid()).addChildEventListener(childEventListener);
            mFirebaseDatabaseReference.child("Room").orderByChild("0").equalTo(mFirebaseUser.getUid()).addChildEventListener(childEventListener);*/

        }

        if(MainController.Gender.equals(""))
        {
            final Intent intent = new Intent(this, JoinActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }

        if(!mLM.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.setTitle("위치정보 확인");
            alert.setMessage("가까운 대화 상대방을 찾기위해 위치정보(GPS)를 활성화 해주세요.");
            alert.show();
        }

    }

    private  void GetUserInfo(final String id)
    {
              mFirebaseDatabaseReference.child("user").child(id.trim()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        UserModel user = dataSnapshot.getValue(UserModel.class);

                        if(user != null) {

                            MainController.Gender2 = user.getGender();
                            MainController.Name2 = user.getName();
                            MainController.Age2 = user.getAge();
                            MainController.Lat2 = user.getLat();
                            MainController.Lon2 = user.getLon();

                            if(ChatActivity.isStart) {
                                ChatActivity.SetDistanceInfo();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("user", "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

      ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {

         /*   String id =  dataSnapshot.child("0").getValue().toString();
            String id2 =  dataSnapshot.child("1").getValue().toString();*/

           //String[] result = dataSnapshot.child("0").getValue().toString().replace("[", "").replace("]","").split(",");

            if(!isChat) {
                String value = dataSnapshot.getKey();
                Log.d("Add", value);

                mFirebaseDatabaseReference.child("Queue").child(mFirebaseUser.getUid()).removeValue();

                startChatActivity(value);


                if (dataSnapshot.hasChildren()) {
                    if (!dataSnapshot.child("0").getValue().toString().equals(mFirebaseUser.getUid())) {
                        GetUserInfo(dataSnapshot.child("0").getValue().toString());
                    } else if (!dataSnapshot.child("1").getValue().toString().equals(mFirebaseUser.getUid())) {
                        GetUserInfo(dataSnapshot.child("1").getValue().toString());
                    }
                }
            }

        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
            String value = dataSnapshot.getKey();
            Log.d("Change", value);
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            String value = dataSnapshot.getKey();
            Log.d("Remove", value);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
            String value = dataSnapshot.getKey();
            Log.d("Move", value);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            String value = databaseError.toString();
            Log.d("databaseError", value);
        }
    };

    private void ShowError()
    {
        Toast.makeText(this, "인증 실패",
                Toast.LENGTH_SHORT).show();
    }


    private  void ShowInAppBilling()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setPositiveButton("구매", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
             mHelper.buy("pray_vip");
            }
        });
        alert.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        alert.setMessage("이 기능을 사용하려면, VIP (30일) 이용권이 필요합니다. 구매하시겠습니까?");
        alert.show();
    }



    private  void SetLocation()
    {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            if(mLM == null)
            {
                mLM = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            }

            List<String> providers = mLM.getProviders(true);



             //mLM.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            for (String provider : providers) {
                Location l = mLM.getLastKnownLocation(provider);
                if (l == null) {
                    String aa = "aa";
                    continue;
                }
                if (location == null || l.getAccuracy() < location.getAccuracy()) {
                    // Found best last known location: %s", l);
                    location = l;
                }
            }

            if(location != null)
            {
                MainController.Lat = location.getLatitude();
                MainController.Lon = location.getLongitude();

                SharedPreferences pref = getSharedPreferences("Profile", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("lat", String.valueOf(MainController.Lat));
                editor.putString("lon", String.valueOf(MainController.Lon));

                editor.commit();
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        isChat = false;

        if (mProgressDialog!=null&&mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }

        if(mFirebaseAuth == null) return;;
        if(mFirebaseDatabaseReference == null) return;
        if(mFirebaseUser == null) return;;

        if(mFirebaseAuth != null && mFirebaseDatabaseReference != null && mFirebaseUser != null)
        {
            SetLocation();

            UserModel user = new UserModel(mFirebaseUser.getUid(), MainController.Name, MainController.Age, MainController.Gender, "On");
            user.setLat(MainController.Lat);
            user.setLon(MainController.Lon);

            java.util.Date today = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String current = formatter.format(today);
            user.setStart_time(current);

            MainController.Start_Time = current;
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/user/" + mFirebaseUser.getUid() , user.toMapBasic());

            mFirebaseDatabaseReference.updateChildren(childUpdates);
        }
    }

    @Override
    protected void onStop() {

        mFirebaseDatabaseReference.child("Queue").child(mFirebaseUser.getUid()).removeValue();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* If a user is currently authenticated, display a logout menu */
        if (mFirebaseAuth != null) {
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    SetLocation();

                } else {

                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
       /* if (id == R.id.action_logout) {
            //logout();
            return true;
        }*/
        if (id == R.id.action_CS) {
           /* final Intent intent = new Intent(this, InAppBillingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);*/

           //mHelper.ConsumePurchaseItems();

   /*         Intent it = new Intent(Intent.ACTION_SEND);
            it.putExtra(Intent.EXTRA_EMAIL, "illhagonolgo@gmail.com");
            it.putExtra(Intent.EXTRA_SUBJECT, "비나이다에게 문의합니다.");
            it.setType("text/plain");
            startActivity(Intent.createChooser(it, "Choose Email Client"));*/

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL,  new String[] { "illhagonolgo@gmail.com" }) ;
            intent.putExtra(Intent.EXTRA_SUBJECT, "비나이다에게 문의합니다.");
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }

        }

        if(id == R.id.action_Setting)
        {
            final Intent intent = new Intent(this, SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }

        if(id == R.id.action_review)
        {
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=com.firewood.pary"));
            startActivity(marketLaunch);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {

        mFirebaseDatabaseReference.child("Queue").child(mFirebaseUser.getUid()).removeValue();
        mFirebaseDatabaseReference.child("Room").orderByChild("1").equalTo(mFirebaseUser.getUid()).removeEventListener(childEventListener);
        mFirebaseDatabaseReference.child("Room").orderByChild("0").equalTo(mFirebaseUser.getUid()).removeEventListener(childEventListener);
        MainController.isRunning = false;
        mHelper.destroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mHelper.activityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 500 && !MainController.VIP)
        {
            if(adCount % 5 == 0)
            {
                ShowAd();
            }
        }
    }

    private void logout() {


        mFirebaseAuth.signOut();
        mFirebaseDatabaseReference.child("user").child(mFirebaseUser.getUid()).removeValue();

        //LoginManager.getInstance().logOut();

        LoginManager.getInstance().logOut();
        final Intent intent = new Intent(this, FaceBookLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);

        finish();
    }


    private  void startChat()
    {
        SetLocation();

        mFirebaseDatabaseReference.child("Room").orderByChild("1").equalTo(mFirebaseUser.getUid()).addChildEventListener(childEventListener);
        mFirebaseDatabaseReference.child("Room").orderByChild("0").equalTo(mFirebaseUser.getUid()).addChildEventListener(childEventListener);

        String value = "아무 대화 상대를 찾는 중...";

        if(mChatType.equals("M"))
        {
            value = "남자 대화 상대를 찾는 중...";
        }
        else if(mChatType.equals("F"))
        {
            value = "여자 대화 상대를 찾는 중...";
        }


        MainController.Gender2 = "";
        MainController.Name2 = "";
        MainController.Age2 = "";
        MainController.Lat2 = 0;
        MainController.Lon2 = 0;


        mProgressDialog = ProgressDialog.show(this, "" , value, true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {


            @Override
            public void onCancel(DialogInterface dialog) {

                if(mFirebaseAuth != null && mFirebaseDatabaseReference != null && mFirebaseUser != null) {
                    mFirebaseDatabaseReference.child("Queue").child(mFirebaseUser.getUid()).removeValue();
                    mFirebaseDatabaseReference.child("Queue").child(mFirebaseUser.getUid()).onDisconnect().removeValue();
                }
            }
        });

        try {


            UserModel user = new UserModel(mFirebaseUser.getUid(), MainController.Name, MainController.Age, MainController.Gender, "On", mChatType);
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/Queue/" + mFirebaseUser.getUid() , user.toMap());
            mFirebaseDatabaseReference.updateChildren(childUpdates);

        }
        catch(Exception e)
        {
            Log.d("Exception", e.toString());

            if (mProgressDialog!=null&&mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
        }
    }

    private  void startChatActivity(String chatKey)
    {
        isChat = true;

        mFirebaseDatabaseReference.child("Room").orderByChild("1").equalTo(mFirebaseUser.getUid()).removeEventListener(childEventListener);
        mFirebaseDatabaseReference.child("Room").orderByChild("0").equalTo(mFirebaseUser.getUid()).removeEventListener(childEventListener);

        if (mProgressDialog!=null&&mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }

        final Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("chat", chatKey);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 500);
    }

    private  void startFreeBoardActivity()
    {
        isChat = true;

        final Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("chat", "freeboard");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, 500);
    }

}
