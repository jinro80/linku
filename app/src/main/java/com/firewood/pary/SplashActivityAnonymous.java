package com.firewood.pary;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firewood.pary.Controller.MainController;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.security.MessageDigest;


public class SplashActivityAnonymous extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseAuth mFirebaseAuth;
    private ProgressDialog mProgressDialog;

   private void ShowDialog()
   {
       mProgressDialog = ProgressDialog.show(this, "" ,"Loading...", true);
   }

    private void HideDialog()
    {
        if (mProgressDialog!=null&&mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash key", something);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("name not found", e.toString());
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_splash);


        Firebase.setAndroidContext(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        initailize();
    }

    private void ShowError()
    {
        Toast.makeText(this, "Authentication failed.",
                Toast.LENGTH_SHORT).show();
    }

    private  void initailize()
    {
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    StartMain();

                } else {
                    // User is signed out
                    signInAnonymously();
                }

            }
        };
    }


    private void signInAnonymously() {
        ShowDialog();
        // [START signin_anonymously]
        mFirebaseAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {


                        StartMain();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            ShowError();
                        }

                        // [START_EXCLUDE]
                        HideDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END signin_anonymously]
    }


    @Override
    public void onStart() {
        super.onStart();
        mFirebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mFirebaseAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private  void StartMain() {

        SharedPreferences pref = getSharedPreferences("Profile", Activity.MODE_PRIVATE);

        if (pref != null) {
            MainController.Gender = pref.getString("gender", "");
            MainController.Age = pref.getString("age", "20");
            MainController.Name = pref.getString("name", "");
            MainController.Lat =  Double.valueOf(pref.getString("lat", "0"));
            MainController.Lon =  Double.valueOf(pref.getString("lon", "0"));
            MainController.usePush = pref.getBoolean("usePush", true);
        }


        if(MainController.Gender.equals(""))
        {
            final Intent intent = new Intent(this, JoinActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }
        else {

            final Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }

        finish();
    }

}
