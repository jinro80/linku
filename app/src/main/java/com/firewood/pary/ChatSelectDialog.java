package com.firewood.pary;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

/**
 * Created by Chris on 2016-12-15.
 */

public class ChatSelectDialog extends Dialog {

    private ImageButton mFemaleButton;
    private ImageButton mMaleButton;



    private View.OnClickListener mFemleClickListener;
    private View.OnClickListener mMaleClickListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.chat_select_dialog);

        mFemaleButton = (ImageButton) findViewById(R.id.femaleButton);
        mMaleButton = (ImageButton) findViewById(R.id.maleButton);


        // 클릭 이벤트 셋팅
        if (mFemleClickListener != null && mMaleClickListener != null) {
            mFemaleButton.setOnClickListener(mFemleClickListener);
            mMaleButton.setOnClickListener(mMaleClickListener);

        }
    }

    public ChatSelectDialog(Context context, View.OnClickListener maleListener,
                        View.OnClickListener femaleListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mFemleClickListener = femaleListener;
        this.mMaleClickListener = maleListener;
    }
}