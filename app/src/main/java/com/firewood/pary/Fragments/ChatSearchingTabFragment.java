package com.firewood.pary.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firewood.pary.R;
import com.firewood.pary.SplashActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chris on 2016-10-17.
 */

public class ChatSearchingTabFragment  extends Fragment {

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mFirebaseDatabaseReference;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.chat_searching_tab_fragment, container, false);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();


        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();


        Button updateTrue = (Button) view.findViewById(R.id.updateTrue);
        updateTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTrue();
            }
        });

        Button updateFalse = (Button) view.findViewById(R.id.updateFalse);

        updateFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFalse();
            }
        });
        return view;
    }

    private void updateTrue()

    {
        if(mFirebaseAuth == null) return;;
        if(mFirebaseDatabaseReference == null) return;
        if(mFirebaseUser == null) return;;

        if(mFirebaseAuth != null && mFirebaseDatabaseReference != null && mFirebaseUser != null)
        {

            Map newUserData = new HashMap();
            newUserData.put("isRead", true);

            mFirebaseDatabaseReference.child("message").child("2").updateChildren(newUserData);

        }
    }

    private  void  updateFalse()
    {
        if(mFirebaseAuth == null) return;;
        if(mFirebaseDatabaseReference == null) return;
        if(mFirebaseUser == null) return;;

        if(mFirebaseAuth != null && mFirebaseDatabaseReference != null && mFirebaseUser != null)
        {
            Map newUserData = new HashMap();
            newUserData.put("isRead", false);

            mFirebaseDatabaseReference.child("message").child("2").updateChildren(newUserData);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        final Intent intent = new Intent(getActivity(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }
}
