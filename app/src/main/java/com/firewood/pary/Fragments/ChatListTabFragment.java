package com.firewood.pary.Fragments;

import android.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firewood.pary.ChatActivity;
import com.firewood.pary.R;
import com.firewood.pary.SplashActivity;

/**
 * Created by Chris on 2016-10-17.
 */

public class ChatListTabFragment extends Fragment {


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.chat_list_tab_fragment,container,false);
        Button startChat = (Button)view.findViewById(R.id.startChat);


        startChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startChat();
            }
        });

        return view;
    }

    private  void startChat()
    {
        final Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        final Intent intent = new Intent(getActivity(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }
}
